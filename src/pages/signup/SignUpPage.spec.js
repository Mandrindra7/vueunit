import SignUp from "./SignUpPage.vue"
import { render, screen } from "@testing-library/vue"
import '@testing-library/jest-dom'

describe("Sign up page", () => {
    describe("Layout", () => {
        it('sign up header ', () => {
            render(SignUp);
            const header = screen.queryByRole('heading', {name: 'Sign up'})
            expect(header).toBeInTheDocument()
        });

        it("has username input", () => {
            render(SignUp);
            const input = screen.queryByLabelText("username")
            expect(input).toBeInTheDocument()
        })

        it("has email input", () => {
            render(SignUp)
            const input = screen.queryByPlaceholderText("e-mail")
            expect(input).toBeInTheDocument()
        })
    })
    
})
